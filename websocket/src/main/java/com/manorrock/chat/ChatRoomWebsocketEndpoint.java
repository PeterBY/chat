/*
 *  Copyright (c) 2002-2016, Manorrock.com. All Rights Reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *      1. Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *
 *      2. Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */
package com.manorrock.chat;

import java.io.IOException;
import java.util.ArrayList;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 * The Websocket endpoint.
 * 
 * @author Manfred Riem (mriem@manorrock.com)
 */
@ServerEndpoint(value = "/room/{room}")
public class ChatRoomWebsocketEndpoint {

    /**
     * Stores the sessions.
     */
    private static final ArrayList<Session> SESSIONS = new ArrayList<>();
    
    /**
     * Open the websocket.
     * 
     * @param session the session.
     * @param room the chat room.
     * @throws IOException when an I/O error occurs.
     */
    @OnOpen
    public void open(Session session, @PathParam("room") String room) throws IOException {
        session.getUserProperties().put("room", room);
        session.getBasicRemote().sendText("Session " + session + " entering room " + room);
        SESSIONS.add(session);
    }
    
    /**
     * Handle close.
     * 
     * @param session the session.
     */
    @OnClose
    public void close(Session session) {
        SESSIONS.remove(session);
    }
    
    /**
     * Handle error.
     * 
     * @param session the session.
     * @param throwable the throwable.
     */
    @OnError
    public void error(Session session, Throwable throwable) {
        SESSIONS.remove(session);
    }

    /**
     * Handle the message.
     * 
     * @param message the message.
     * @param session the session.
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        String room = (String) session.getUserProperties().get("room");
        try {
            for (Session currentSession : SESSIONS) {
                if (currentSession.isOpen()
                        && room.equals(currentSession.getUserProperties().get("room"))) {
                    currentSession.getBasicRemote().sendText(message);
                }
            }
        } catch (IOException ioe) {
        }
    }
}
